#! /usr/bin/python

fileIN = open("../../strumm_patterns.txt", "r")
out="byte pattern[] = {"
for line in fileIN:

    beat = 1

    if "+" in line:
        outbyte = 0
    else:
        outbyte = 1

    for char in line:
        if char == ".":
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "x":
            outbyte = outbyte | 126
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "1":
            outbyte = outbyte | 2
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "2":
            outbyte = outbyte | 4
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "3":
            outbyte = outbyte | 8
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "4":
            outbyte = outbyte | 16
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "5":
            outbyte = outbyte | 32
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "6":
            outbyte = outbyte | 64
            out += hex(outbyte) + ","
            outbyte = 0
            beat +=1
        elif char == "+":
            outbyte = outbyte | 1
        elif char == "-":
            outbyte = outbyte | 128

    out += "\n"
out += "};"

print out
        
