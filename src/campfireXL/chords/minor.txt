C#m                      [x 4 6 6 5 4] (C# E  G#) : minor triad 
C#m                      [x 4 6 6 x 0] (C# E  G#) : minor triad 
C#m                      [x x 2 1 2 0] (C# E  G#) : minor triad 
C#m7                     [0 2 2 1 2 0] (C# E  G# B) : minor triad, minor 7th 
C#m7                     [x 4 6 4 5 4] (C# E  G# B) : minor triad, minor 7th 
Cm                       [x 3 5 5 4 3] (C  D# G) : minor triad 
Cm                       [x x 5 5 4 3] (C  D# G) : minor triad 
Cm6                      [x x 1 2 1 3] (C  D# G  A) : minor triad plus 6th 
Cm7                      [x 3 5 3 4 3] (C  D# G  A#) : minor triad, minor 7th 
D#m                      [x x 4 3 4 2] (D# F# A#) : minor triad 
D#m7                     [x x 1 3 2 2] (C# D# F# A#) : minor triad, minor 7th 
Dm                       [x 0 0 2 3 1] (D  F  A) : minor triad 
Dm6                      [1 2 3 2 3 1] (D  F  A  B) : minor triad plus 6th 
Dm6                      [x 2 0 2 0 1] (D  F  A  B) : minor triad plus 6th 
Dm6                      [x x 0 2 0 1] (D  F  A  B) : minor triad plus 6th 
Dm7                      [x 5 7 5 6 5] (C  D  F  A) : minor triad, minor 7th 
Dm7                      [x x 0 2 1 1] (C  D  F  A) : minor triad, minor 7th 
Dm7                      [x x 0 5 6 5] (C  D  F  A) : minor triad, minor 7th 
Dm7/add11 or   Dm7/11    [3 x 0 2 1 1] (C  D  F  G  A) : minor triad, minor 7th, plus 11th
Dmin/maj7                [x x 0 2 2 1] (C# D  F  A) : minor triad, major 7th 
Em                       [0 2 2 0 0 0] (E  G  B) : minor triad 
Em                       [3 x 2 0 0 0] (E  G  B) : minor triad 
Em                       [x 2 5 x x 0] (E  G  B) : minor triad 
Em6                      [0 2 2 0 2 0] (C# E  G  B) : minor triad plus 6th 
Em7                      [0 2 0 0 0 0] (D  E  G  B) : minor triad, minor 7th 
Em7                      [0 2 0 0 3 0] (D  E  G  B) : minor triad, minor 7th 
Em7                      [0 2 2 0 3 0] (D  E  G  B) : minor triad, minor 7th 
Em7                      [0 2 2 0 3 3] (D  E  G  B) : minor triad, minor 7th 
Em7                      [0 x 0 0 0 0] (D  E  G  B) : minor triad, minor 7th 
Em7                      [x 10 12 12 12 0] (D  E  G  B) : minor triad, minor 7th 
Em7                      [x x 0 0 0 0] (D  E  G  B) : minor triad, minor 7th 
Em7                      [x x 0 12 12 12] (D  E  G  B) : minor triad, minor 7th 
Em7                      [x x 0 9 8 7] (D  E  G  B) : minor triad, minor 7th 
Em7                      [x x 2 4 3 3] (D  E  G  B) : minor triad, minor 7th 
Em7/add11 or   Em7/11    [0 0 0 0 0 0] (D  E  G  A  B) : minor triad, minor 7th, plus 11th
Em7/add11 or   Em7/11    [0 0 0 0 0 3] (D  E  G  A  B) : minor triad, minor 7th, plus 11th
Em7/add11 or   Em7/11    [3 x 0 2 0 0] (D  E  G  A  B) : minor triad, minor 7th, plus 11th
Em9                      [0 2 0 0 0 2] (D  E  F# G  B) : minor triad, minor 7th plus 9th
Em9                      [0 2 0 0 3 2] (D  E  F# G  B) : minor triad, minor 7th plus 9th
Em9                      [2 2 0 0 0 0] (D  E  F# G  B) : minor triad, minor 7th plus 9th
Emin/maj7                [3 x 1 0 0 0] (D# E  G  B) : minor triad, major 7th 
Emin/maj7                [x x 1 0 0 0] (D# E  G  B) : minor triad, major 7th 
Emin/maj9                [0 6 4 0 0 0] (D# E  F# G  B) : minor triad, major 7th plus 9th 
F#m                      [2 4 4 2 2 2] (C# F# A) : minor triad 
F#m                      [x 4 4 2 2 2] (C# F# A) : minor triad 
F#m                      [x x 4 2 2 2] (C# F# A) : minor triad 
F#m7                     [0 0 2 2 2 2] (C# E  F# A) : minor triad, minor 7th 
F#m7                     [0 x 4 2 2 0] (C# E  F# A) : minor triad, minor 7th 
F#m7                     [2 x 2 2 2 0] (C# E  F# A) : minor triad, minor 7th 
F#m7                     [x 0 4 2 2 0] (C# E  F# A) : minor triad, minor 7th 
F#m7                     [x x 2 2 2 2] (C# E  F# A) : minor triad, minor 7th 
F#m7/b9                  [0 0 2 0 2 2] (C# E  F# G  A) : minor triad, minor 7th flat 9th 
Fm                       [x 3 3 1 1 1] (C  F  G#) : minor triad 
Fm                       [x x 3 1 1 1] (C  F  G#) : minor triad 
Fm6                      [x x 0 1 1 1] (C  D  F  G#) : minor triad plus 6th 
Fm7                      [x 8 10 8 9 8] (C  D# F  G#) : minor triad, minor 7th 
Fm7                      [x x 1 1 1 1] (C  D# F  G#) : minor triad, minor 7th 
G#m                      [x x 6 4 4 4] (D# G# B) : minor triad 
G#m7                     [x x 4 4 4 4] (D# F# G# B) : minor triad, minor 7th 
Gm                       [3 5 5 3 3 3] (D  G  A#) : minor triad 
Gm                       [x x 0 3 3 3] (D  G  A#) : minor triad 
Gm13                     [0 0 3 3 3 3] (D  E  F  G  A  A#) : minor triad, minor 7th, plus 9th and 13th
Gm6                      [3 x 0 3 3 0] (D  E  G  A#) : minor triad plus 6th 
Gm7                      [3 5 3 3 3 3] (D  F  G  A#) : minor triad, minor 7th 
Gm7                      [x x 3 3 3 3] (D  F  G  A#) : minor triad, minor 7th 
Gm7/add11 or   Gm7/11    [x 3 3 3 3 3] (C  D  F  G  A#) : minor triad, minor 7th, plus 11th 
Gm9                      [3 5 3 3 3 5] (D  F  G  A  A#) : minor triad, minor 7th plus 9th 
A#m                      [1 1 3 3 2 1] (C# F  A#) : minor triad 
A#m7                     [x 1 3 1 2 1] (C# F  G# A#) : minor triad, minor 7th 
Am                       [8 12 x x x 0] (C  E  A) : minor triad 
Am                       [x 0 2 2 1 0] (C  E  A) : minor triad 
Am                       [x 0 7 5 5 5] (C  E  A) : minor triad 
Am                       [x 3 2 2 1 0] (C  E  A) : minor triad 
Am6                      [x 0 2 2 1 2] (C  E  F# A) : minor triad plus 6th 
Am6                      [x x 2 2 1 2] (C  E  F# A) : minor triad plus 6th 
Am7                      [0 0 2 0 1 3] (C  E  G  A) : minor triad, minor 7th 
Am7                      [x 0 2 0 1 0] (C  E  G  A) : minor triad, minor 7th 
Am7                      [x 0 2 2 1 3] (C  E  G  A) : minor triad, minor 7th 
Am7                      [x 0 5 5 5 8] (C  E  G  A) : minor triad, minor 7th 
Am7/add11 or   Am7/11    [x 5 7 5 8 0] (C  D  E  G  A) : minor triad, minor 7th, plus 11th
Amin/maj9                [x 0 6 5 5 7] (C  E  G# A  B) : minor triad, major 7th plus 9th
Bm                       [2 2 4 4 3 2] (D  F# B) : minor triad 
Bm                       [x 2 4 4 3 2] (D  F# B) : minor triad 
Bm                       [x x 0 4 3 2] (D  F# B) : minor triad 
Bm7                      [x 0 4 4 3 2] (D  F# A  B) : minor triad, minor 7th 
Bm7                      [x 2 0 2 0 2] (D  F# A  B) : minor triad, minor 7th 
Bm7                      [x 2 0 2 3 2] (D  F# A  B) : minor triad, minor 7th 
Bm7                      [x 2 4 2 3 2] (D  F# A  B) : minor triad, minor 7th 
Bm7                      [x x 0 2 0 2] (D  F# A  B) : minor triad, minor 7th 
Bm7/add11 or   Bm7/11    [0 0 2 4 3 2] (D  E  F# A  B) : minor triad, minor 7th, plus 11th 
Bm7/add11 or   Bm7/11    [0 2 0 2 0 2] (D  E  F# A  B) : minor triad, minor 7th, plus 11th 
