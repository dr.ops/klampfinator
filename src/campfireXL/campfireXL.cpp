#include "Fluxamasynth.h"
#include <LiquidCrystal.h>
#include "TimerOne.h"
#include <EEPROM.h>
#include <WString.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4 // not used / nicht genutzt bei diesem Display
Adafruit_SSD1306 display(OLED_RESET);


Fluxamasynth synth;          // create our synth

// pointer into EEPROM Memory (4096 Bytes)
const int patternbase = 0; // room for 128 patterns of 32 byte each 

const int power[]={1,2,4,8,16,32,64}; // powers of two (precalculated for speed)              

#define SELPIN 49 //Selection Pin 
#define DATAOUT 51//MOSI 
#define DATAIN  50//MISO 
#define SPICLOCK  52//Clock 
int readvalue; 

int playmode = 2;
int playmodecounter = 2;
int b = 0;
int menu = 0;
int inSub = 0;
int devnul = 0;
int rhythmstrings[12][16] = {0};
byte chord[9][6];
byte keypressed = 0;
byte fingersSet = 0;
byte mode = 0;
byte oldmode = 0;
char screen[4][25];

byte guitarstring[6]={40,45,50,55,59,64};
byte oldstring[] = {0,0,0,0,0,0};
byte basetone = 43;
byte mmp = 1;
byte velocity = 64;
byte counter = 200;
int patternpointer = 0 ;
int patterncount = 4 ;
byte pointer = 0;
byte strokepointer=0;
byte rakespeed = 3;
byte volume = 0;
byte volumeval = 0;
byte righthand = 0;
int s=0;
String line1, line2, line3, line4;

// The following array represents one rhythm per line, as 'rhythm' could
// also mean "Some generic rhythm in some scale" as the first 24 do.
// When a rhythm is selected in the menu up to 7 chords (could be up to 16)
// are mapped to a key on the lefthand keypad 
// for 'generic' rhythms these are the chords of standard scale
// (I, II, III, IV, V, VI, VII)
// in  maj, min, min, min, maj, maj, min for major scales
// and a shifted scale for minor scales
// each of the 7 numbers in the these chord arrays is a pointer 
// into the global chord array 
// (eg. a 3 here means line 3 in the chord array which is a G Chord)
struct rhythm{  byte number; char text[16]; byte patterns[16];} rhythms[] ={
  {0,"Walking",  {00,11,12,13,14,15,21,22,23,24,25,26}},
  {1,"WalkPick", {00,01,02,03,04,05,21,22,23,24,25,26}},
  {2,"Univers",  {00,01,02,03,04,05,27,28,29,30,31,32}}
  {3,"Waltz",    {00,06,07,08,09,10,16,17,18,19,20,32}}
  };

int read_poti(byte channel){
 
   byte dataMSB =    0;
  byte dataLSB =    0;
  byte JUNK    = 0x00;
  
  digitalWrite         (SELPIN, LOW);
  SPI.transfer         (0x01);                                 // Start Bit
  dataMSB =            SPI.transfer((channel | 0x08) << 4) & 0x03;  // Send readAddress and receive MSB data, masked to two bits
  dataLSB =            SPI.transfer(JUNK);                     // Push junk data and get LSB byte return
  digitalWrite         (SELPIN, HIGH);
 
  return               dataMSB << 8 | dataLSB;
}

void waitforrelease(){
  while (digitalRead(22)==LOW or digitalRead(24)==LOW or digitalRead(26)==LOW or digitalRead(36)==LOW or digitalRead(38)==LOW or digitalRead(40)==LOW){;}

}

// the following function sets the fingers of the left hand
// to a specific chord. the chord is set

void setFingers(int acc) {

  if (playmode == 11){strokepointer = -1;}
  if (counter == 200){counter = 0;}
  for (byte i=0; i<6; i++) {
    guitarstring[i] = chord[acc][i] ;
  }

	if (playmode == 2) {
	  counter = 0;
	  strokepointer = 0;
	}

	if (playmode == 2) {
	  waitforrelease();
	}

  display.clearDisplay();
 
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0,0);
  display.println(line1);

  display.setCursor(0,8);
  display.println(line2);

  display.setCursor(0,16);
  display.println(line3);

  display.setCursor(0,24);
  display.println(line4);
  display.display();

}


void initFingers(int acc) {

  for (byte i=0; i<6; i++) {
    guitarstring[i] = chord[acc][i] ;
  }
}

void createMainDisplay(){
  String notenames = "C C#D D#E F F#G G#A A#H ";
  String output ="";
  
  int note;
  for(int i=0;i<7;i++){
    note = chord[i][0];
	output = output + "  " +notenames.substring((note % 12)*2,((note % 12)*2)+2)  ;
	}
  line4 = "";
  line4 = output.substring(0,15);
  line3 = output.substring(16,27);
  line2 = notenames.substring((chord[0][0] % 12)*2,((chord[0][0] % 12)*2)+2) + " ";

  if (mmp == 1){line2 = line2 + "maj ";}
  else if (mmp == 2){line2 = line2 + "min ";}

  
 
   
}
  
void setChord(){
  int octbase = 0;
  if (mmp == 1){                        // major mode
	
	octbase = basetone / 12;
	octbase = octbase * 12;
	  
    chord[0][0] = octbase + ((((basetone + 0) % 12) + 8) % 12) +4;
	chord[1][0] = octbase + ((((basetone + 2) % 12) + 8) % 12) +4;
	chord[2][0] = octbase + ((((basetone + 4) % 12) + 8) % 12) +4;
	chord[3][0] = octbase + ((((basetone + 5) % 12) + 8) % 12) +4;
	chord[4][0] = octbase + ((((basetone + 7) % 12) + 8) % 12) +4;
	chord[5][0] = octbase + ((((basetone + 9) % 12) + 8) % 12) +4;
	chord[6][0] = octbase + ((((basetone + 11) % 12) + 8) % 12) +4;
	chord[7][0] = octbase + ((((basetone + 5) % 12) + 8) % 12) +4;

	chord[0][1] = chord[0][0] +7;   
	chord[1][1] = chord[1][0] +7;   
    chord[2][1] = chord[2][0] +7;   
	chord[3][1] = chord[3][0] +7;
	chord[4][1] = chord[4][0] +7; 
	chord[5][1] = chord[5][0] +7; 
	chord[6][1] = chord[6][0] +6;
	chord[7][1] = chord[7][0] +7;

	chord[0][2] = chord[0][0] +12; 
	chord[1][2] = chord[1][0] +12;
	chord[2][2] = chord[2][0] +12;
	chord[3][2] = chord[3][0] +12; 
	chord[4][2] = chord[4][0] +12;
	chord[5][2] = chord[5][0] +12; 
	chord[6][2] = chord[6][0] +12; 
	chord[7][2] = chord[7][0] +12; 

	chord[0][3] = chord[0][0] +16; 
	chord[1][3] = chord[1][0] +15; 
	chord[2][3] = chord[2][0] +15; 
	chord[3][3] = chord[3][0] +16; 
	chord[4][3] = chord[4][0] +16; 
	chord[5][3] = chord[5][0] +15; 
	chord[6][3] = chord[6][0] +15; 
	chord[7][3] = chord[7][0] +15; 

	chord[0][4] = chord[0][0] +19; 
	chord[1][4] = chord[1][0] +19; 
	chord[2][4] = chord[2][0] +19; 
	chord[3][4] = chord[3][0] +19; 
	chord[4][4] = chord[4][0] +19; 
	chord[5][4] = chord[5][0] +19; 
	chord[6][4] = chord[6][0] +18; 
	chord[7][4] = chord[7][0] +19; 

	chord[0][5] = chord[0][0] +24; 
	chord[1][5] = chord[1][0] +24; 
	chord[2][5] = chord[2][0] +24; 
	chord[3][5] = chord[3][0] +24; 
	chord[4][5] = chord[4][0] +24; 
	chord[5][5] = chord[5][0] +24; 
	chord[6][5] = chord[6][0] +24; 
	chord[7][5] = chord[7][0] +24; 

  }
  if (mmp ==2){                    // minor mode
    chord[0][0] = octbase + ((((basetone + 0) % 12) + 8) % 12) +4;
	chord[1][0] = octbase + ((((basetone + 2) % 12) + 8) % 12) +4;
	chord[2][0] = octbase + ((((basetone + 3) % 12) + 8) % 12) +4;
	chord[3][0] = octbase + ((((basetone + 5) % 12) + 8) % 12) +4;
	chord[4][0] = octbase + ((((basetone + 7) % 12) + 8) % 12) +4;
	chord[5][0] = octbase + ((((basetone + 8) % 12) + 8) % 12) +4;
	chord[6][0] = octbase + ((((basetone + 10) % 12) + 8) % 12) +4;

	chord[0][1] = chord[0][0] +7;
	chord[1][1] = chord[1][0] +6;
	chord[2][1] = chord[2][0] +7;
	chord[3][1] = chord[3][0] +7;
	chord[4][1] = chord[4][0] +7;
	chord[5][1] = chord[5][0] +7;
	chord[6][1] = chord[6][0] +7;

	chord[0][2] = chord[0][0] +12; 
	chord[1][2] = chord[1][0] +12; 
	chord[2][2] = chord[2][0] +12; 
	chord[3][2] = chord[3][0] +12; 
	chord[4][2] = chord[4][0] +12; 
	chord[5][2] = chord[5][0] +12; 
	chord[6][2] = chord[6][0] +12; 

  	chord[0][3] = chord[0][0] +15; 
	chord[1][3] = chord[1][0] +15; 
	chord[2][3] = chord[2][0] +16; 
	chord[3][3] = chord[3][0] +15; 
	chord[4][3] = chord[4][0] +15; 
	chord[5][3] = chord[5][0] +16; 
	chord[6][3] = chord[6][0] +16; 

	chord[0][4] = chord[0][0] +19; 
	chord[1][4] = chord[1][0] +18;
	chord[2][4] = chord[2][0] +19; 
	chord[3][4] = chord[3][0] +19; 
	chord[4][4] = chord[4][0] +19; 
	chord[5][4] = chord[5][0] +19; 
	chord[6][4] = chord[6][0] +18; 

	chord[0][5] = chord[0][0] +24; 
	chord[1][5] = chord[1][0] +24; 
	chord[2][5] = chord[2][0] +24; 
	chord[3][5] = chord[3][0] +24; 
	chord[4][5] = chord[4][0] +24; 
	chord[5][5] = chord[5][0] +24; 
	chord[6][5] = chord[6][0] +24; 
}	

  createMainDisplay();
  display.clearDisplay();
	
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0,0);
  display.println(line1);

  display.setCursor(0,8);
  display.println(line2);

 display.setCursor(0,16);
 display.println(line3);

 display.setCursor(0,24);
 display.println(line4);
 display.display();

}

void setCube() {

  int a = 0;
  byte lo =0;
  byte hi = 0;
  byte p = 0;
  
  for(int r=0;r<12;r++){
    for(int b=0;b<16;b++){
      p = rhythms[patternpointer].patterns[r];
    
      lo = EEPROM.read(patternbase+p*32+b*2);
      hi = EEPROM.read(patternbase+p*32+b*2+1);
      a = word(hi,lo);
      rhythmstrings[r][b] = a; 
    }
  }

  display.clearDisplay();
	
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0,0);
  display.println(line1);

  display.setCursor(0,8);
  display.println(line2);

 display.setCursor(0,16);
 display.println(line3);

 display.setCursor(0,24);
 display.println(line4);
 display.display();


}

void makeMenu(byte adv){
  if (inSub == true){
	  

  }
  else {  // *** MainMenu
	if (adv == 0){menu++; if (menu == 5){menu = 0;}}
	

	if (menu == 0){
	display.clearDisplay();
	display.setTextColor(WHITE);
	display.setTextSize(1);
	display.setCursor(0,0);
	display.println("Klampfinator");
	display.setCursor(0,8);
	display.println(basetone);
	display.setCursor(0,16);
	display.println("github.com:");
	display.setCursor(0,24);
	display.println("andydrop/klampfinator");
	display.display();
	  }
	else if (menu == 1){
	  line1 = "Base Tone";
	  if (adv == 1){basetone++; if (basetone == 128){basetone = 0;}}
	  if (adv == 2){basetone--; if (basetone == 0){basetone = 127;}}
	  setChord();
	}
	else if (menu == 2){
	  line1 = "Major/Minor";
	  if (adv == 1){mmp++; if (mmp > 2){mmp = 1;}}
	  if (adv == 2){mmp--; if (mmp < 1){mmp = 2;}}
	  setChord();
	} 
	else if (menu == 3){
	  line1 = "Patterns";
	  if (adv == 1){patternpointer++; if (patternpointer == patterncount){patternpointer = 0;}}
	  if (adv == 2){patternpointer--; if (patternpointer < 0){patternpointer = patterncount -1;}}
	  line2 = rhythms[patternpointer].text;
	  setCube();
	} 
	else if (menu == 4){
	  line1 = "Playmode";
	  if (adv == 1){playmode++; if (playmode > playmodecounter){playmode = 1;}}
	  if (adv == 2){playmode--; if (playmode < 1){playmode = playmodecounter;}}
      display.clearDisplay();
      display.setTextColor(WHITE);
      display.setTextSize(1);
      display.setCursor(0,0);
      display.println(line1);
      display.setCursor(0,8);
      display.println(line2);
     display.setCursor(0,16);
     display.println(line3);
     display.setCursor(0,24);
     display.println(line4);
     display.display();
	} 

  }

   
 
	waitforrelease();
}




void pling() {
     int test = 0;
	 int playvol =0;

     if ((counter ==  0) or (counter==3) or (counter==6) or (counter==9) or (counter==12) or (counter==15)){
	   if (strokepointer != 16) {  
		 volume = volumeval;
		 if ((rhythmstrings[righthand][strokepointer] & 1) != 0) { volume *= 1.2;}

		 b = counter / 3;//for (b=0;b<6;b++){

     	    // select the string to play
     	    if ((rhythmstrings[righthand][strokepointer] & 256) != 0 )  { 
     	       s = b ;     //for downstrokes
     	         }
     	    else {
			s = 5 - b; // for upstrokes
			}      

     	    // maybe play the selected string 
     	    test = power[s+1]; // get bitvalue to test for
     	    // if the pattern want's to play a string...
     	    if ((rhythmstrings[righthand][strokepointer] & test) != 0) {
     	       if (oldstring[s] != 0) {         // if a tone is already played on that string...
	       	  synth.noteOff(s,oldstring[s]); // stop the old tone (otherwise timy-whimey wibbely-wobbly things will happen)
			   }
			   if ( s < 3)
				 {playvol = volume; }
			   else
				 {playvol = volume; }
			   synth.noteOn(s,guitarstring[s] ,playvol); // then play the new tone
			   oldstring[s]=guitarstring[s];           // and remember it for stopping it later
			}
	   }
     } else if (counter != 32 and counter > 0 and counter < velocity){
     } else if (counter == 32){

       // Mute Strings if bit 7 is set
       if ((rhythmstrings[righthand][strokepointer] & 128) != 0) { // if bit 7 is set
       	  for (s=0; s<6; s++){                                             // for each string
	      if (oldstring[s] != 0) {                                       // if it plays a note
	      	 synth.noteOff(s,oldstring[s]);                               // turn it off
	      }
      	  }   
       }  

	 } else {  // counter >= velocity
	   if (playmode == 2) {
		   counter = -1;
		   if ((rhythmstrings[righthand][strokepointer] & 512) != 0) { // if bit 9 is set
			 strokepointer = 15; // restart the loop immediately
		   }
		   strokepointer++;
		   if (strokepointer > 15){strokepointer=16;}
		 }
	   else {
		   counter = -1;
		   if ((rhythmstrings[righthand][strokepointer] & 512) != 0) { // if bit 9 is set
			 strokepointer = -1; // restart the loop immediately
		   }
		   strokepointer++;
		   if (strokepointer > 15){strokepointer=0;}
	   }
	 }
     counter ++;

}

void setup() {

 // initialize with the I2C addr 0x3C / mit I2C-Adresse 0x3c initialisieren
 display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
 
 // random start seed / zufälligen Startwert für Random-Funtionen initialisieren
 randomSeed(analogRead(0));
 
 #define DRAW_DELAY 118
 #define D_NUM 47
 
	display.clearDisplay();
	
	display.setTextColor(WHITE);
	display.setTextSize(1);
	display.setCursor(0,0);
	display.println("Klampfinator 1.1");

	display.setCursor(0,8);
	display.println("andy@remline.de");

	display.setCursor(0,16);
	display.println("github.com:");

	display.setCursor(0,24);
	display.println("andydrop/klampfinator");

	display.display();

 //set SPI pin modes 
  SPI.begin     ();
  pinMode       (SELPIN, OUTPUT);
  digitalWrite  (SELPIN, LOW);        // Cycle the ADC CS pin as per datasheet
  digitalWrite  (SELPIN, HIGH);


  // start communication with the synth-shield
  Serial.begin(31250);       // Set MIDI baud rate:

  // set instruments to the first 6 channels (for each of the 6 strings)
  synth.programChange((byte)0, (byte)0, (byte)24); // steel-string
  synth.programChange((byte)0, (byte)1, (byte)24); // steel-string
  synth.programChange((byte)0, (byte)2, (byte)24); // steel-string
  synth.programChange((byte)0, (byte)3, (byte)24); // nylon-string
  synth.programChange((byte)0, (byte)4, (byte)24); // nylon-string
  synth.programChange((byte)0, (byte)5, (byte)24); // nylon-string

  synth.programChange((byte)0, (byte)9, (byte)29); // percussion-channel für MenuMode 
  synth.setChannelVolume(9, 127);

  // prepare some inputs channels for the keyboard
  pinMode(22, INPUT_PULLUP);      
  pinMode(24, INPUT_PULLUP);     
  pinMode(26, INPUT_PULLUP);     
  pinMode(28, OUTPUT);     
  pinMode(30, OUTPUT);     
  pinMode(32, OUTPUT);     
  pinMode(34, OUTPUT);     
  pinMode(36, INPUT_PULLUP);     
  pinMode(38, INPUT_PULLUP);     
  pinMode(40, INPUT_PULLUP);     
  
  digitalWrite(28,1);
  digitalWrite(30,1);
  digitalWrite(32,1);
  digitalWrite(34,1);

  setChord();
  setCube();
  initFingers(0);

  // now, after everything is set up the 'pling' routine
  // is attched to the timer1 interrupt and gets called every
  // ~ 2ms. 
  Timer1.initialize(1953); // 1.953125 ms or 31250 Cycles
  Timer1.attachInterrupt(pling);
}

void loop() {

  if (mode == 0){
    setCube();
    mode = 99;
  }
  else if (mode == 99){ // *** PLAY-Mode *** 

	digitalWrite(28,0); // Zeile 4
	  if (digitalRead(22)==LOW){righthand = 0;}
	  if (digitalRead(24)==LOW){righthand = 4;}
	  if (digitalRead(26)==LOW){righthand = 8;}
	  if (digitalRead(36)==LOW){setFingers(0);}
	  if (digitalRead(38)==LOW){setFingers(4);}
	  if (digitalRead(40)==LOW){setFingers(7);}
	  digitalWrite(28,1);

	  digitalWrite(30,0); // Zeile 3
	  if (digitalRead(22)==LOW){righthand = 1;}
	  if (digitalRead(24)==LOW){righthand = 5;}
	  if (digitalRead(26)==LOW){righthand = 9;}
	  if (digitalRead(36)==LOW){setFingers(1);}
	  if (digitalRead(38)==LOW){setFingers(5);}
	  if (digitalRead(40)==LOW){setFingers(8);}
	  digitalWrite(30,1);

	  digitalWrite(32,0); // Zeile 2
	  if (digitalRead(22)==LOW){righthand = 2;}
	  if (digitalRead(24)==LOW){righthand = 6;}
	  if (digitalRead(26)==LOW){righthand = 10;}
	  if (digitalRead(36)==LOW){setFingers(2);}
	  if (digitalRead(38)==LOW){setFingers(6);}
	  if (digitalRead(40)==LOW){makeMenu(2);} // -
	  digitalWrite(32,1);

	  digitalWrite(34,0); // Zeile 1
	  if (digitalRead(22)==LOW){righthand = 3;}
	  if (digitalRead(24)==LOW){righthand = 7;}
	  if (digitalRead(26)==LOW){righthand = 11;}
	  if (digitalRead(36)==LOW){setFingers(3);}
	  if (digitalRead(38)==LOW){makeMenu(0);} // OK
	  if (digitalRead(40)==LOW){makeMenu(1);} // +
	  digitalWrite(34,1);
   
  }
    // the velocity is set by a variable resistor knob which 
    // gives us a value between 0 an 1023
    // dividing it by 16 makes it go from 0 to 63
  velocity = read_poti(1)/16; //2=Unten 0=Vorne 1=Oben
    // adding 64 makes it go from 64 to 127
    // 64 interrupts of ~ 2 ms are ~ 1/8 second for every stroke
    // 127 interrupts of ~ 2 ms are ~ 1/4 second for every stroke
    // 4 strokes make a beat (for 4/4 or 3/4 rhythms) so the resulting
    // speed is between 60 (velocity 127) and 120 (velocity 64) beats per minute
    // Attention: low velocity value means higher bpm
    velocity += 48;
    volumeval = read_poti(0)/10 ;//2=Unten 0=Vorne 1=Oben
    rakespeed = read_poti(2)/128 ;//2=Unten 0=Vorne 1=Oben

}

