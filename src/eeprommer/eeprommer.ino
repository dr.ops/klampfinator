#include <EEPROM.h>

const int patternbase = 0; // room for 109 patterns of 32 byte each 
const int maxpattern = 128; // room for 109 patterns of 32 byte each 


/* Bitpattern for each of the stroke:
//
// Bit 0 strings are played  20% louder (e.g. for emphasizing rhythm)
//
// Bit 1 String 1 (lowest sounding string)
// Bit 2 String 2
// Bit 3 String 3
// Bit 4 String 4
// Bit 5 String 5
// Bit 6 String 6 (highest sounding string)
// 
// Bit 7 Strings are muted  (either right after play, or on an otherwise silent stroke)
// 
// Bit 8 Strings are played as a downstroke (otherwise upstroke, useless for single notes)
// Bit 8 Strings are played as a downstroke (otherwise upstroke, useless for single notes)
*/

struct pattern{char strings[42];} patterns[] = {
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 00 Silence
  // Strumming 4/4
  {"d+,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 01 1/1 Stroke
  {"d+,_,_,_,_,_,_,_,d,_,_,_,_,_,_,_"},  // 02 1/2 Stroke
  {"d+,_,_,_,d,_,_,_,d,_,_,_,d,_,_,_"},  // 03 1/4 Stroke
  {"d+,_,u,_,d,_,u,_,d,_,u,_,d,_,u,_"},  // 04 1/8 Stroke
  {"d+,u,d,u,d,u,d,u,d,u,d,u,d,u,d,u"},  // 05 1/16 Stroke 
  // Strumming Waltz
  {"d+,_,_,_,_,_,_,_,_,_,_,0,_,_,_,_"},           // 06 1/1 Stroke
  {"d+,_,_,_,_,_,_,_,u,_,_,0,_,_,_,_"},          // 07 1/2 Stroke
  {"d+,_,_,_,u,_,_,_,d,_,_,0,_,_,_,_"},         // 08 1/4 Stroke
  {"d+,_,u,_,d,_,u,_,d,_,u,0,_,_,_,_"},       // 09 1/8 Stroke
  {"d+,u,d,u,d,u,d,u,d,u,d,0,_,_,_,_"},  // 10 1/16 Stroke 
  // Picking
  {"1+,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 11 1/1 Stroke
  {"1+,_,_,_,_,_,_,_,2,_,_,_,_,_,_,_"},  // 12 1/2 Stroke
  {"1+,_,_,_,2,_,_,_,3,_,_,_,2,_,_,_"},  // 13 1/4 Stroke
  {"1+,_,2,_,3,_,2,_,1,_,2,_,3,_,2,_"},  // 14 1/8 Stroke
  {"1+,2,3,2,1,2,3,2,1,2,3,2,1,2,3,2"},  // 15 1/16 Stroke 
  //Picking Waltz
  {"d+,_,_,_,_,_,_,_,_,_,_,0,_,_,_,_"},           // 16 1/1 Stroke
  {"d+,_,_,_,_,_,_,_,u,_,_,0,_,_,_,_"},          // 17 1/2 Stroke
  {"d+,_,_,_,u,_,_,_,d,_,_,0,_,_,_,_"},         // 18 1/4 Stroke
  {"d+,_,_,_,d,_,u,_,d,_,u,0,_,_,_,_"},       // 19 1/8 Stroke
  {"d+,u,d,u,d,u,d,u,d,u,d,0,_,_,_,_"},  // 20 1/16 Stroke 

  // Walking
  {"1+,_,_,_,h,_,_,_,2+,_,_,_,h,_,_,_"}, // 21 Boom Chick
  {"1+,_,_,_,h,_,h^,_,2+,_,_,_,h,_,h^,_"}, // 22 Boom Chicka
  {"1+,_,_,_,h-,_,_,_,2+,_,_,_,h-,_,_,_"}, // 23 Boom Chick
  {"3+,_,_,_,h-,_,_,_,2+,_,_,_,h-,_,_,_"}, // 24 Boom Chick
  {"3+,_,_,_,h,_,_,_,2+,_,_,_,h,_,_,_"}, // 25 Boom Chick
  {"3+,_,_,_,h,_,h^,_,2+,_,_,_,h,_,h^,_"}, // 26 Boom Chicka

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 27 Folk Rhythm
  {"d+,_,_,_,d,_,u-,_,_,_,u-,_,d,_,u-,_"}, // 28 Folk Rhythm
  {"d+,_,_,_,d-,_,u-,_,_,_,u-,_,d-,_,u-,_"}, // 29 Folk Rhythm
  {"d+,_,_,_,d,_,u,_,_,_,u,_,d,_,u,_"}, // 30 Folk Rhythm
  {"d+,_,_,_,d,_,u,_,_,_,u,_,d,_,u,_"}, // 27 Folk Rhythm
  {"d+,_,_,_,d,_,u,_,_,_,u,_,d,_,u,_"}, // 27 Folk Rhythm

  // ToDo
  {"d+,_,_,u,_,u,_,u,d,_,_,u,d,u,d,u"}, // 33 Folk Rhythm
  {"d+,_,_,u,_,u,_,u,d,_,_,u,d,u,d,u"}, // 34 Folk Rhythm
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 35  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 36  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 37  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 38  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 39 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 40  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 41  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 42  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 43  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 44  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 45 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 46  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 47  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 48  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 49  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 50  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 51 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 52 7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 53  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 54  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 55  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 56  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 57 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 58  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 59  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 60  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 61  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 62  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 63 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 64  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 65  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 66  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 67  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 68  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 69 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 70  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 71  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 72  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 73  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 74  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 75 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 76  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 77  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 78  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 79  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 80  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 81 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 82 7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 83  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 84  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 85  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 86  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 87 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 88  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 89  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 90  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 91  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 92  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 93 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 94  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 95  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 96  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 97  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 98  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 99 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 100  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 101  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 102  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 103  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 104  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 105 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 106  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 107  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 108  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 109  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 110  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 111 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 112 7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 113  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 114  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 115  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 116  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 117 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 118  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 119  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 120  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 121  7/8
  {"D+,_,_,_,U,_,D-,_,_,_,U-,_,_,0,_,_"},  // 122  7/8

  // ToDo
  {"D+,_,_,_,D,_,U,_,_,_,U,_,D,_,U,_"}, // 123 Folk Rhythm
  {"D+,_,_,_,_,_,D+,_,_,_,U,_,_,0,_,_"},  // 124  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U,_,_,0,_,_"},  // 125  7/8
  {"D+,_,_,_,_,_,D+,_,_,_,U-,_,_,0,_,_"},  // 126  7/8
  {"D+,_,_,_,U,_,D+,_,_,_,U-,_,_,0,_,_"},  // 127  7/8
  
  {"1+,_,4,5,3,6,4,_,1,_,4,5,3,6,4,_"}, // 12 Silence
  {"D+,_,D,U,D,U,D,_,D,_,D,U,D,U,D,_"}, // 13 Silence
  {"1,2,3,4,5,6,_,_,1,2,3,4,5,6,_,_"},  // 14 Silence
  {"B,4,5,4,6,4,5,4,B,4,5,4,6,4,5,4"},  // 15 Silence
  {"U+,_,U,_,_,U,D,U,_,D,_,_,U,_,U,_"},  // 16 Chöre
  {"D+,_,_,_,D,_,U,_,D,_,_,_,D,_,U,_"},  // 17 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 18 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 19 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 20 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 21 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 22 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 23 Silence

  {"B+,_,H,_,B,H,_,H,B+,_,H,B,H,_,H,_"}, // 24 Bossa Nova
  {"B+,_,H-,_,B,H,_,H-,B+,_,H,B,H-,_,H,_"}, // 25 Bossa Nova
  {"B+,_,H-,_,B+,H-,_,H-,B+,_,H-,B,H-,_,H-,_"},// 26 Bossa Nova
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 27 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 28 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 29 Silence

  {"+S,_,_,_,N,_,_,_,N,_,_,0,_,_,_,_"},  // 30 Waltz
  {"+S,_,_,_,N,_,_,_,N-,_,_,0,_,_,_,_"},  // 31 Waltz
  {"+S,_,_,_,N-,_,_,_,N-,_,_,0,_,_,_,_"},  // 32 Waltz
  {"+S,_,4,_,5,_,4,_,6,_,4,0,_,_,_,_"},  // 33 Waltz
  {"+B,_,4,_,5,_,4,_,6,_,4,0,_,_,_,_"},  // 34 Waltz
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 35 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"},  // 36 Silence
  {"_,_,_,_,_,_,_,_,_,_,_,_,_,_,_,_"}   // 37 Silence
};

int encodePattern(char*  pattern, int beat){
  int result = 0;
  char strings[9] = "";
  byte i;
  byte stringptr = 0;
  byte bar = 0;

  for (i=0;i<128;i++){
    if (pattern[i]  == ',' or pattern[i] == '\0'){
      bar++;
    }
    else if (beat == bar) {
      strings[stringptr] = pattern[i];
      stringptr++;
    }
  }
  strings[stringptr] = '\0';

  for (i=0;strings[i] != '\0';i++){
    if (((strings[i] >= 'A') and  (strings[i] <= 'Z')) or (strings[i] == '+'))  {result = result | 1;} //play louder
    else if (strings[i] == '_'){result = result | 0;} //do nothing
    else if (strings[i] == '1'){result = result | 2;} //lowest string
    else if (strings[i] == '2'){result = result | 4;}
    else if (strings[i] == '3'){result = result | 8;}
    else if (strings[i] == '4'){result = result | 16;}
    else if (strings[i] == '5'){result = result | 32;}
    else if (strings[i] == '6'){result = result | 64;} // highest string
    else if (strings[i] == '-'){result = result | 128;} // mute string
    else if (strings[i] == '^'){result = result | 256;} //upwards
    else if (strings[i] == '0'){result = result | 512;} // End of Loop. Start Again from Here
    else if (toupper(strings[i]) == 'J'){result = result | 60;} // all Steel Strings
    else if (toupper(strings[i]) == 'K'){result = result | 6;} // 2 (lower)Steel Strings
    else if (toupper(strings[i]) == 'L'){result = result | 14;} // all (lower)Steel Strings
    else if (toupper(strings[i]) == 'M'){result = result | 30;} //4 (lower) Strings
    else if (toupper(strings[i]) == 'N'){result = result | 62;} // 5 (lower)Steel Strings
    else if (toupper(strings[i]) == 'F'){result = result | 124;} // all Nylon Strings
    else if (toupper(strings[i]) == 'G'){result = result | 120;} // all Nylon Strings
    else if (toupper(strings[i]) == 'H'){result = result | 112;} // all Nylon Strings
    else if (toupper(strings[i]) == 'I'){result = result | 96;} // all Nylon Strings
    else if (toupper(strings[i]) == 'O'){result = result | 56;} // Higher middle3
    else if (toupper(strings[i]) == 'P'){result = result | 78;} // Powerchord
    else if (toupper(strings[i]) == 'Q'){result = result | 28;} // lower middle3
    else if (toupper(strings[i]) == 'B'){result = result | 2;} // base note
    else if (toupper(strings[i]) == 'U'){result = result | 124;} // upstroke
    else if (toupper(strings[i]) == 'D'){result = result | 382;} // Downstroke
    }
 
  return result;
}




// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(13, OUTPUT);   

  
  int a = 0;
  byte lo = 0;
  byte hi = 0;
  for(int p=0;p<maxpattern;p++){
    for(int beat=0;beat<16;beat++){
      a = encodePattern(patterns[p].strings,beat);
      hi = highByte(a);
      lo = lowByte(a);
      EEPROM.write(patternbase+(p*32)+(beat*2),lo);
      EEPROM.write(patternbase+(p*32)+(beat*2)+1,hi);
    }
  }  


}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(200);               // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(200);               // wait for a second
}
